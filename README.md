# Wrike time log on commit hook

Make sure you have `python` installed and available via command line. Do `pip install requirement.txt`.

## Instructions to use
1. Get a wrike permanent access token and add it to `wrike_config.json`, refer Permanent token section at bottom of [https://developers.wrike.com/oauth-20-authorization/](https://developers.wrike.com/oauth-20-authorization/)
2. Run `./install` to make `logtime-wrike` accessible as a command.
3. Use `logtime-wrike '.;duration1 #ticket-number1 "comment1";duration2 #ticket-number2 "comment2"'`
4. Every `;` (semicolon) starts a new time entry.
   - Each option in an entry is space separated.
   - `duration` is mandatory to be entered in hours and should be the first option in entry right after `;`. eg: `'.;2 #12345'` valid, `'.;#1234 2'` invalid
   - `#` starts the ticket number if not mentioned. Defaults to `#default`, which is specified in `wrike_config.json`.
   - By default, the date is taken as today. If you want to log for a previous day, use `-` followed by the number of days ago. For example, `-1` for yesterday.
   - If one entry specifies a ticket and the following entry doesn't then the ticket number from previous entry is used.
   - Comment should be withint `"` double quotes. If comment is not given then the commit message before the first `;` is used as a comment.
   - Refer `wrike_parser_test.py` for more examples
5. Run `./install -Repository {repository_path}` to install hook into a repo.
6. Run `./install -Uninstall -Repository {repository_path}` to remove hook from a repo.
7. Run `./install -Uninstall` to remove all environment variables too.
