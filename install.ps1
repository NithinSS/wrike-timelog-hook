param (
    [switch]$Uninstall,
    [string]$Repository
)

if ($Uninstall -and -not $Repository) {
    Write-Warning "Removing wrike variables"
    [Environment]::SetEnvironmentVariable("WRIKE_CONFIG", $null, "User")
    [Environment]::SetEnvironmentVariable("WRIKE_CONFIG", $null, "Process")
    [Environment]::SetEnvironmentVariable("WRIKE_HOOK_SCRIPT", $null, "User")
    [Environment]::SetEnvironmentVariable("WRIKE_HOOK_SCRIPT", $null, "Process")
    Write-Output "All wrike variable are now removed from environment"
    return
} elseif (-not $Uninstall) {
    if ($env:WRIKE_CONFIG) {
        $config_path = $env:WRIKE_CONFIG
        if (-not (Test-Path $config_path)) {
            Write-Warning "WRIKE_CONFIG path is not accessible, please check the path"
        }
    } else {
        Write-Warning "WRIKE_CONFIG was not found, adding it"
        [Environment]::SetEnvironmentVariable("WRIKE_CONFIG", "$PWD\wrike_config.json", "User")
        [Environment]::SetEnvironmentVariable("WRIKE_CONFIG", "$PWD\wrike_config.json", "Process")
    }

    if ($env:WRIKE_HOOK_SCRIPT) {
        $hook_script_path = [Environment]::GetEnvironmentVariable("WRIKE_HOOK_SCRIPT", "User")
        if (-not (Test-Path $hook_script_path)) {
            Write-Warning "WRIKE_HOOK_SCRIPT path is not accessible, please check the path"
        }
    } else {
        Write-Warning "WRIKE_HOOK_SCRIPT was not found, adding it"
        [Environment]::SetEnvironmentVariable("WRIKE_HOOK_SCRIPT", "$PWD\wrike_hook.py", "User")
        [Environment]::SetEnvironmentVariable("WRIKE_HOOK_SCRIPT", "$PWD\wrike_hook.py", "Process")
    }

    if ($env:Path -notlike "*$PWD*") {
        Write-Warning "Current directory was not seen in PATH, adding it..."
        $env:Path += ";$PWD"
        $user_path = [Environment]::GetEnvironmentVariable("Path", "User")
        $user_path += ";$PWD"
        [Environment]::SetEnvironmentVariable("Path", $user_path, "User")
        Write-Output "You should be able to do `logtime-wrike 'timelog string';` to add timelog now"
    }
}

if (-not $Repository) {
    Write-Warning "No repository path provided. Exiting."
    Write-Output "Usage: ./install -Repository <repository_path>"
    Write-Output "repository_path: path to the git repository to install the hook to"
    return
}

if (-not (Test-Path "$Repository\.git")) {
    Write-Warning "The provided path is not a git repository. Exiting."
    return
}

if ($Uninstall -and $Repository) {
    if (Test-Path "$Repository\.git\hooks\commit-msg") {
        Write-Warning "Removing hook from $Repository, to remove environment variables use `./install -Uninstall;` without a -Repository"
        Remove-Item "$Repository\.git\hooks\commit-msg" -Force
        Write-Output "Hook uninstalled from $Repository"
    } else {
        Write-Warning "Looks like a hook is not installed to $Repository"
    }
    return
}

$hook_path = Join-Path $Repository ".git\hooks\commit-msg"
Copy-Item -Path "commit-msg" -Destination $hook_path -Force
Write-Output "Hook installed to $Repository"