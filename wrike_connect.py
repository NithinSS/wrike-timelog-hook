import requests
import json
import datetime
import os
import sys

try:
    CONFIG_PATH = os.environ['WRIKE_CONFIG']
    with open(CONFIG_PATH) as config_file:
        wrike_config = json.load(config_file)
except KeyError:
    print('WRIKE_CONFIG not found in environment variables, aborting...')
    sys.exit(1)

try: 
    WRIKE_TOKEN = wrike_config['token']
    WRIKE_BASE_URL = wrike_config['baseUrl']
    WRIKE_SETTINGS = wrike_config['settings']
    if "continueOnTimeout" in WRIKE_SETTINGS and WRIKE_SETTINGS["continueOnTimeout"] == True:
        CONTINUE_ON_TIMEOUT = True
except KeyError as error:
    print(f"Please update the wrike config at {os.environ['WRIKE_CONFIG']} to include {error}. Aborting... ")
    sys.exit(1)

session = requests.Session()

def get_user():
    if not WRIKE_TOKEN or len(WRIKE_TOKEN) == 0:
        print(f'"token" for Wrike is not correct, please update {CONFIG_PATH}')
        sys.exit(1)
        
    headers = {
        "Authorization": f"Bearer {WRIKE_TOKEN}",
        "Content-Type": "application/json"
    }
    
    url = WRIKE_BASE_URL + "/contacts?me"
    try:
        # start_time = time.time()
        response = session.get(url, headers=headers, timeout=30)
        response.raise_for_status()
        response_json = response.json()
        # print(f"Time to response {time.time() - start_time}")
        if "data" in response_json and len(response_json["data"]) > 0 and "firstName" in response_json["data"][0]:
            return [response_json["data"][0]["id"], response_json["data"][0]["firstName"]]
        else:
            raise ValueError('Name of wrike user not found')
    except requests.exceptions.Timeout:
        print(f"TIMEOUT: Cannot connect to {url}")
        if CONTINUE_ON_TIMEOUT:
            print("Timeout ignored, skipping timelog, continuing commit...")
            sys.exit(0)
        else:
            print("Timeout cannot be ignored, set 'continueOnTimeout' settings in JSON true to continue or remove the timelog string.")
            sys.exit(1)
    except requests.exceptions.HTTPError as error:
        print(error)
        sys.exit(1)


def get_time_log_aggregate(user_id: str, start_date: str, end_date: str):
    headers = {
        "Authorization": f"Bearer {WRIKE_TOKEN}",
        "Content-Type": "application/json"
    }
    url = WRIKE_BASE_URL + f"/contacts/{user_id}/timelogs?trackedDate={{\"start\":\"{start_date}\",\"end\":\"{end_date}\"}}"
    
    try:
        response = session.get(url, headers=headers)
        response.raise_for_status()
        response_json = response.json()
        if "data" in response_json and len(response_json["data"]) > 0:
            return sum([log["hours"] for log in response_json["data"]])
        else:
            return 0
    except requests.exceptions.Timeout:
        print(f"TIMEOUT: Cannot connect to {url}")
        if CONTINUE_ON_TIMEOUT:
            print("Timeout ignored, skipping timelog, continuing commit...")
            sys.exit(0)
        else:
            print("Timeout cannot be ignored, set 'continueOnTimeout' settings in JSON true to continue or remove the timelog string.")
            sys.exit(1)
    except requests.exceptions.HTTPError as error:
        print(error)
        sys.exit(1)
    
    
def get_task_from_link(permalink: str):
    url = WRIKE_BASE_URL + f"/tasks?permalink={permalink}"
    headers = {
        "Authorization": f"Bearer {WRIKE_TOKEN}",
        "Content-Type": "application/json"
    }
    
    try:
        response = session.get(url, headers=headers)
        response.raise_for_status()
        response_json = response.json()
        if "data" in response_json and len(response_json["data"]) > 0 and "id" in response_json["data"][0]:
            task_id = response_json["data"][0]["id"]
            task_name = response_json["data"][0]["title"]
        else:
            raise ValueError('Task ID not found in response')
        return [task_id, task_name]
    except requests.exceptions.Timeout:
        print(f"TIMEOUT: Cannot connect to {url}")
        if CONTINUE_ON_TIMEOUT:
            print("Timeout ignored, skipping timelog, continuing commit...")
            sys.exit(0)
        else:
            print("Timeout cannot be ignored, set 'continueOnTimeout' settings in JSON true to continue or remove the timelog string.")
            sys.exit(1)
    except requests.exceptions.HTTPError as error:
        print(error)
        sys.exit(1)
    
    
def create_timelog(task_id:  str, comment: str, hours: float, tracked_date: str):
    url = WRIKE_BASE_URL + f"/tasks/{task_id}/timelogs"
    headers = {
        "Authorization": f"Bearer {WRIKE_TOKEN}",
        "Content-Type": "application/json"
    }
    
    if task_id is None:
        raise ValueError('Task ID cannot be empty')
    
    if hours is None or hours == 0:
        raise ValueError('Duration cannot be empty')
    
    if tracked_date is None:
        tracked_date = datetime.date.today().strftime("%Y-%m-%d")
        
    if comment is None:
        comment = ''
        
    data = {
        "comment": comment,
        "hours": hours,
        "trackedDate": tracked_date,
        "plainText": "true"
    }
    
    
    try:
        response = session.post(url, headers=headers, data=json.dumps(data))
        response.raise_for_status()
        # print(response.json())
    except requests.exceptions.Timeout:
        print(f"TIMEOUT: Cannot connect to {url}")
        if CONTINUE_ON_TIMEOUT:
            print("Timeout ignored, skipping timelog, continuing commit...")
            sys.exit(0)
        else:
            print("Timeout cannot be ignored, set 'continueOnTimeout' settings in JSON true to continue or remove the timelog string.")
            sys.exit(1)
    except requests.exceptions.HTTPError as error:
        print(error)
        sys.exit(1)


def create_timelog_for_permalink(task_permalink:  str, comment: str, hours: float, tracked_date: str):
    [task_id, task_name] = get_task_from_link(task_permalink)
    print(f"\"{comment}\" for {hours} hrs under {task_name} ({task_permalink}) on {tracked_date}.")
    create_timelog(task_id, comment, hours, tracked_date)
    