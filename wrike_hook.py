from wrike_connect import CONFIG_PATH, wrike_config, create_timelog_for_permalink, get_user, get_time_log_aggregate
from wrike_parser import parse_commit_message
import datetime
import sys


tracking_dates = set()

def execute_tasks(tasks):
  for task in tasks:
    permalink_id = task['task'][1:] # remove '#' at start
    if permalink_id in wrike_config['tasks']:
      saved_permalink = wrike_config['tasks'][permalink_id]
      if not saved_permalink and len(saved_permalink.strip()) == 0:
        print(f'Please update your Wrike config JSON ({CONFIG_PATH}) to include "{permalink_id}" under "tasks". Aborting...')
        sys.exit(1)
      
      permalink = saved_permalink
    else:
      permalink = wrike_config['permalinkPrefix'] + permalink_id
    
    today = datetime.datetime.now()
    task_datetime = today if task['date'] == 0 else (today + datetime.timedelta(days=task['date']))
    task_date_str = task_datetime.strftime('%Y-%m-%d')
    tracking_dates.add(task_date_str)
    
    create_timelog_for_permalink(
      permalink,
      task['comment'],
      task['hours'],
      task_date_str
    )
    

def print_aggregated_times(user_id: str):
  for date_str in tracking_dates:
    date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    day_plus_one = date + datetime.timedelta(days=1)
    day_plus_one_str = day_plus_one.strftime('%Y-%m-%d')
    aggregate_time = get_time_log_aggregate(user_id, date_str, day_plus_one_str)
    print(f"Total of {aggregate_time}hrs recorded for {date_str}")
  
  
if __name__ == "__main__":
    commit_message = sys.argv[1]
    tasks = parse_commit_message(commit_message)
    if len(tasks) > 0:
      [user_id, user_name] = get_user()
      print(f"You are recording wrike timelog as {user_name}")
      execute_tasks(tasks)
      print_aggregated_times(user_id)


    