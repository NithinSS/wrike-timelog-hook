import unittest

from wrike_parser import parse_commit_message

class TestProcessCommitMessage(unittest.TestCase):

    def test_case_1(self):
        input_message = "some text"
        expected_output = []
        self.assertEqual(parse_commit_message(input_message), expected_output)

    def test_case_2(self):
        input_message = "some other text;"
        expected_output = []
        self.assertEqual(parse_commit_message(input_message), expected_output)

    def test_case_3(self):
        input_message = "some other text ;"
        expected_output = []
        self.assertEqual(parse_commit_message(input_message), expected_output)

    def test_case_4(self):
        input_message = "text blah blah;1"
        expected_output = [{"hours": 1, "comment": "text blah blah", "task": "#default", "date": 0}]
        self.assertEqual(parse_commit_message(input_message), expected_output)

    def test_case_5(self):
        input_message = "text blah blah; 1.5"
        expected_output = [{"hours": 1.5, "comment": "text blah blah", "task": "#default", "date": 0}]
        self.assertEqual(parse_commit_message(input_message), expected_output)
        
    def test_case_6(self):
        input_message = 'some text; 2.5 #17826786 -1;'
        expected_output = [{"hours": 2.5, "comment": "some text", "task": "#17826786", "date": -1}]
        self.assertEqual(parse_commit_message(input_message), expected_output)

    def test_case_7(self):
        input_message = 'some comment; 2.5 #17826786 -1;1'
        expected_output = [{"hours": 2.5, "comment": "some comment", "task": "#17826786", "date": -1}, {"hours": 1, "comment": "some comment", "task": "#17826786", "date": 0}]
        self.assertEqual(parse_commit_message(input_message), expected_output)

    def test_case_8(self):
        input_message = 'commit comment;2.5 #17826786 "some other comment";'
        expected_output = [{"hours": 2.5, "comment": "some other comment", "task": "#17826786", "date": 0}]
        self.assertEqual(parse_commit_message(input_message), expected_output)

    def test_case_9(self):
        input_message = 'commit comment;2.5 #17826786 "some other comment" -1;'
        expected_output = [{"hours": 2.5, "comment": "some other comment", "task": "#17826786", "date": -1}]
        self.assertEqual(parse_commit_message(input_message), expected_output)

    def test_case_10(self):
        input_message = 'commit comment;2.5 #17826786 "some other comment" -1;1.5 "some other comment"'
        expected_output = [{"hours": 2.5, "comment": "some other comment", "task": "#17826786", "date": -1}, 
                           {"hours": 1.5, "comment": "some other comment", "task": "#17826786", "date": 0}]
        self.assertEqual(parse_commit_message(input_message), expected_output)
        
    def test_case_11(self):
        input_message = 'commit comment;2.5 #17826786 "some other comment" -1;1.5 "some other comment"; 2 "a new comment" #1564;2.25 -1'
        expected_output = [{"hours": 2.5, "comment": "some other comment", "task": "#17826786", "date": -1}, 
                           {"hours": 1.5, "comment": "some other comment", "task": "#17826786", "date": 0},
                           {"hours": 2.0, "comment": "a new comment", "task": "#1564", "date": 0},
                           {"hours": 2.25, "comment": "commit comment", "task": "#1564", "date": -1}]
        self.assertEqual(parse_commit_message(input_message), expected_output)
        

if __name__ == '__main__':
    unittest.main()
