import shlex

def parse_commit_message(commit_message):
    tasks = []
    task = None
    
    if len(commit_message) == 0:
        return tasks
    
    msg_split = getNonEmptySplit(commit_message, ';')
    if len(msg_split) <= 1:
        return tasks
    else:
        [commit_comment, task_strings] = [msg_split[0], msg_split[1:]]

        active_task_name = "#default"
        for task_string in task_strings:
            task_string_split = shlex.split(task_string)
            [duration, task_string_split] = [task_string_split[0], task_string_split[1:]]
            [task_name, task_comment, task_date] = parse_task(task_string_split)
            active_task_name = task_name if task_name is not None else active_task_name
            comment = task_comment if task_comment is not None else commit_comment
            
            task = {}
            task['hours'] = float(duration)
            task['comment'] = comment
            task['task'] = active_task_name
            task['date'] = task_date
            tasks.append(task)
        return tasks

def parse_task(task_string_split: list[str]):
    task_name = None
    comment = None
    date = 0
    
    for token in task_string_split:
        if token.startswith('#'):
            task_name = token
        elif token.startswith('-'):
            date = int(token)
        else:
            comment = token.strip()
    return [task_name, comment, date]


def getNonEmptySplit(str, splitSym):
    return [s.strip() for s in str.split(splitSym) if s.strip() != '']

